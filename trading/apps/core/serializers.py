import logging

from django.conf import settings

from rest_framework import serializers
import requests

from apps.core.models import Trade
from apps.core.validators import validate_symbol

logger = logging.getLogger(__name__)


class TradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trade
        fields = ['pk', 'sell_currency', 'sell_amount', 'buy_currency', 'buy_amount', 'date_booked', 'rate']


class RateSerializer(serializers.Serializer):
    sell_currency = serializers.CharField(max_length=3, validators=[validate_symbol])
    buy_currency = serializers.CharField(max_length=3, validators=[validate_symbol])
    rate = serializers.DecimalField(max_digits=22, decimal_places=12, read_only=True)

    def get_rate(self):
        params = {
            'access_key': settings.FIXER_API_KEY,
            'base': self.validated_data['sell_currency'],
            'symbols': self.validated_data['buy_currency'],
        }
        response = requests.get(f'{settings.FIXER_API_URL}/latest', params)
        if response.status_code != 200:
            logger.error(
                f'Unable to fetch rate for '
                f'{self.validated_data["sell_currency"]}/{self.validated_data["buy_currency"]} : {response.text}'
            )
            raise serializers.ValidationError(f'Unable to fetch rate, try again')

        json_response = response.json()
        if not json_response['success']:
            raise serializers.ValidationError(f'{json_response["error"]}')

        rate = json_response['rates'][self.validated_data['buy_currency']]
        self.validated_data['rate'] = rate


class StringListField(serializers.ListField):
    child = serializers.CharField()


class SymbolsSerializer(serializers.Serializer):
    symbols = StringListField(required=False)
