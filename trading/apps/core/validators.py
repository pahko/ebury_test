from django.conf import settings

from rest_framework import serializers


def validate_symbol(symbol):
    if symbol not in settings.SYMBOLS:
        raise serializers.ValidationError(f"Invalid Symbol {symbol}")
