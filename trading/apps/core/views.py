from django.conf import settings

from rest_framework import mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.core.models import Trade
from apps.core.serializers import TradeSerializer, RateSerializer, SymbolsSerializer


class TradeViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Trade.objects.all().order_by('-date_booked')
    serializer_class = TradeSerializer


@api_view(['POST'])
def rates_view(request):
    serializer = RateSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.get_rate()
    return Response(serializer.data)


@api_view(['GET'])
def symbols_view(request):
    serializer = SymbolsSerializer(data={'symbols': settings.SYMBOLS})
    serializer.is_valid()
    return Response(serializer.data)
