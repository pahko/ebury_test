import uuid

import django.db.models


class Trade(django.db.models.Model):
    id = django.db.models.CharField(primary_key=True, editable=False, max_length=9)
    sell_currency = django.db.models.CharField(max_length=3)
    sell_amount = django.db.models.DecimalField(max_digits=16, decimal_places=6)
    buy_currency = django.db.models.CharField(max_length=3)
    buy_amount = django.db.models.DecimalField(max_digits=16, decimal_places=6)
    rate = django.db.models.DecimalField(max_digits=22, decimal_places=12)
    date_booked = django.db.models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = django.db.models.DateTimeField(auto_now=True, editable=False)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.pk = f'TR{uuid.uuid4().hex[:7].upper()}'
        super(Trade, self).save(*args, **kwargs)
