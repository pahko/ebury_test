import pytest


@pytest.fixture(scope='module')
def api_client():
    from rest_framework.test import APIClient
    return APIClient()
