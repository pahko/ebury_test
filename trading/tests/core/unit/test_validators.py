from rest_framework.serializers import ValidationError
import pytest

from apps.core.validators import validate_symbol


@pytest.mark.parametrize('symbol', ['FKP', 'GBP', 'GEL'])
def test_validate_symbol_valid(symbol):
    assert validate_symbol(symbol) is None


@pytest.mark.parametrize('symbol', ['', None, 'INVALID'])
def test_validate_symbol_invalid(symbol):
    with pytest.raises(ValidationError):
        validate_symbol(symbol)
