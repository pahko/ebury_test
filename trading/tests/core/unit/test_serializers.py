from requests.models import Response
from rest_framework.serializers import ValidationError
import mock
import pytest

from apps.core.serializers import RateSerializer


class TestRateSerializer(object):
    serializer_class = RateSerializer

    def test_get_rate_500_error(self):
        data = {'sell_currency': 'USD', 'buy_currency': 'KZT'}
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        path = 'apps.core.serializers.requests.get'
        with mock.patch(path, spec=Response, return_value=mock.MagicMock(status_code=500)) as requests_mock, \
                pytest.raises(ValidationError):
            serializer.get_rate()

        requests_mock.assert_called_once()
        assert 'rate' not in serializer.validated_data

    def test_get_rate_200_error(self):
        data = {'sell_currency': 'USD', 'buy_currency': 'KZT'}
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        path = 'apps.core.serializers.requests.get'
        json_value = {'success': False, 'error': 'pay for the API!'}
        return_value = mock.MagicMock(status_code=200, json=mock.MagicMock(return_value=json_value))
        with mock.patch(path, spec=Response, return_value=return_value) as requests_mock, \
                pytest.raises(ValidationError):
            serializer.get_rate()

        requests_mock.assert_called_once()
        assert 'rate' not in serializer.validated_data

    def test_get_rate_200_with_rate(self):
        data = {'sell_currency': 'USD', 'buy_currency': 'KZT'}
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        path = 'apps.core.serializers.requests.get'
        json_value = {'success': True, 'rates': {'KZT': 1.1}}
        return_value = mock.MagicMock(status_code=200, json=mock.MagicMock(return_value=json_value))
        with mock.patch(path, spec=Response, return_value=return_value) as requests_mock:
            serializer.get_rate()

        requests_mock.assert_called_once()
        assert 'rate' in serializer.validated_data
