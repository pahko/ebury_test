from django.conf import settings
from django.urls import reverse

from requests.models import Response
import mock
import pytest

from apps.core.models import Trade


class TestTtradesView(object):
    url = reverse('trade-list')

    @pytest.mark.django_db
    def test_get_trades(self, api_client):
        trade1 = Trade.objects.create(sell_currency='USD', sell_amount=1, buy_currency='EUR', buy_amount=1, rate=1)
        trade2 = Trade.objects.create(sell_currency='USD', sell_amount=1, buy_currency='MXN', buy_amount=1, rate=1)

        response = api_client.get(self.url)

        assert response.status_code == 200
        assert response.data[0]['pk'] == trade2.pk
        assert response.data[1]['pk'] == trade1.pk

    @pytest.mark.django_db
    def test_post_trades(self, api_client):
        data = {'sell_currency': 'USD', 'sell_amount': 1, 'buy_currency': 'EUR', 'buy_amount': 1, 'rate': 1.0}

        response = api_client.post(self.url, data, format='json')

        assert response.status_code == 201
        assert response.data['pk']


class TestSymbolsView(object):
    url = reverse('symbols-view')

    def test_get_symbols(self, api_client):
        response = api_client.get(self.url)

        assert response.status_code == 200
        assert 'symbols' in response.data
        assert response.data['symbols'] == settings.SYMBOLS


class TestRatesView(object):
    url = reverse('rates-view')

    def test_fixer_error(self, api_client):
        data = {'sell_currency': 'USD', 'buy_currency': 'EUR'}

        path = 'apps.core.serializers.requests.get'
        json_value = {'success': False, 'error': 'error: pay for the API!'}
        return_value = mock.MagicMock(status_code=200, json=mock.MagicMock(return_value=json_value))
        with mock.patch(path, spec=Response, return_value=return_value) as requests_mock:
            response = api_client.post(self.url, data, format='json')

        requests_mock.assert_called_once()
        assert response.status_code == 400
        assert 'error' in response.data[0]

    def test_get_rate(self, api_client):
        data = {'sell_currency': 'USD', 'buy_currency': 'EUR'}

        path = 'apps.core.serializers.requests.get'
        json_value = {'success': True, 'rates': {'EUR': 0.8}}
        return_value = mock.MagicMock(status_code=200, json=mock.MagicMock(return_value=json_value))
        with mock.patch(path, spec=Response, return_value=return_value) as requests_mock:
            response = api_client.post(self.url, data, format='json')

        requests_mock.assert_called_once()
        assert response.status_code == 200
        assert response.data['sell_currency'] == data['sell_currency']
        assert response.data['buy_currency'] == data['buy_currency']
        assert response.data['rate']
