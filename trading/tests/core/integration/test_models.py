import pytest

from apps.core.models import Trade


@pytest.mark.django_db
def test_trade_pk():
    trade = Trade.objects.create(
        sell_currency='USD',
        sell_amount=1.1,
        buy_currency='EUR',
        buy_amount=1,
        rate=0.8,
    )
    assert trade.pk
    assert len(trade.pk) == 9
    assert trade.pk.startswith('TR')
