var app = new Vue({
    el: '#app',
    data: {
        trades: [],
        symbols: [],
        mainView: true,
        mainViewClass: {'d-none': false},
        newTradeClass: {'d-none': true},
        rate: 0.0,
        sellCurrency: "",
        buyCurrency: "",
        sellAmount: undefined,
        buyAmount: undefined,
        responseError: "",
        errorReponseClass: {'d-none': true},
    },
    mounted() {
        let that = this;
        console.log('iniciado!');
        $.get( "/api/trades/", function( response ) {
            that.trades = response
        });
        $.get( "/api/symbols/", function( response ) {
            that.symbols = response.symbols
        });
    },
    methods: {
        displayView: function(event){
            if (event != undefined) {
                event.preventDefault();
            }
            this.mainView = !this.mainView;
            this.mainViewClass = {'d-none': !this.mainView};
            this.newTradeClass = {'d-none': this.mainView};
            this.rate = 0;
            this.sellCurrency = "";
            this.buyCurrency = "";
            this.sellAmount = undefined;
            this.buyAmount = undefined;
        },
        getBuySymbols: function() {
            let that = this;
            return this.symbols.filter(function (e) {
                return e != that.sellCurrency;
            });
        },
        resetError: function(){
            this.errorReponseClass = {'d-none': true};
            this.responseError = "";
        },
        getRate: function(event){
            if (this.sellCurrency != "" && this.buyCurrency != "") {
                this.rate = 0;
                this.resetError();
                let that = this;
                let data = {sell_currency: this.sellCurrency, buy_currency: this.buyCurrency};
                $.post( "/api/rates/", data, function( response ) {
                    that.rate = response.rate;
                    that.calculateBuyAmount();
                }).fail(function(response){
                    that.errorReponseClass = {'d-none': false}
                    that.responseError = response.responseJSON;
                });
            }
        },
        calculateBuyAmount: function(){
            if (this.rate != 0 && this.sellAmount != undefined) {
                this.buyAmount = this.rate * this.sellAmount;
            }
        },
        saveTrade: function(event){
            if (this.sellCurrency != "" && this.buyCurrency != "") {
                this.resetError();
                let that = this;
                let data = {
                    sell_currency: this.sellCurrency,
                    buy_currency: this.buyCurrency,
                    buy_amount: parseFloat(this.buyAmount).toFixed(6),
                    sell_amount: parseFloat(this.sellAmount).toFixed(6),
                    rate: this.rate
                };
                $.post( "/api/trades/", data, function( response ) {
                    that.trades.unshift(response);
                    that.rate = 0;
                    that.displayView();
                }).fail(function(response){
                    that.errorReponseClass = {'d-none': false}
                    that.responseError = response.responseJSON;
                    that.rate = 0;
                });
            }
        }
    }
});
