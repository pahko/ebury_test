
## Trading APP Ebury Test

#### Set-Up

Assuming [Docker]([https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)) is installed and a fixer API key is in place in `trading/.env.sample` (if you don't have one, you can get one from https://fixer.io/product), run:

```bash
cp trading/.env.sample trading/.env
docker-compose up
```
then go to http://localhost:8001/

:)

#### Run test
```bash
docker-compose run trading_api ./run_tests.sh
```
